import localforage from 'localforage';
let LF = {
  testModule: "custom Module LF is working",
  init: function() {
    // ask permission for persistant storage
    // options to make storage persistant: https://developers.google.com/web/updates/2016/06/persistent-storage
    if (navigator.storage && navigator.storage.persist) {
      navigator.storage.persist().then(granted => {
        if (granted) {
          console.log('Storage will not be cleared except by explicit user action');
        } else {
          console.log('Storage may be cleared by the UA under storage pressure.');
          alert('Storage may be cleared by the UA under storage pressure.');
        }
      });
    }

    this.waitForInit();
    localforage.config({
      driver: [localforage.INDEXEDDB],
      name: 'LayersDataBase',
      storeName: 'InitialStoreName'
    })
    //when local forage is fully initialized, log driver or error
    localforage.ready().then(function() {
      let div = document.getElementById('waitingDiv');
      div.parentNode.removeChild(div);
    }).catch(function(e) {
      console.log(e); // `No available storage method found.`
      // One of the cases that `ready()` rejects,
      // is when no usable storage driver is found
    })
  },

  /**
   * gets an item from an object store by ID
   * @param {string} id ID of desired feature
   */
  getItem: function(id) {
    return new Promise((resolve, reject) => {
      localforage.getItem(id).then(function(value) {
        //let item = JSON.parse(value);
        resolve(value);
      }).catch(function(err) {
        console.log(err);
        reject(err);
      });
    })
  },

  addFeature: function(featureId, feature) {
    localforage.setItem(featureId, feature)
        .then(function(value) {
          // This will output the value object (array)
          console.log('object saved. Id: ', featureId);
        })
        .catch(function(err) {
          // This code runs if there were any errors
          console.log('error when saving object via LocalForage');
          console.log('featureId: ', featureId);
          console.log('feature: ', feature);
          console.log(err);
        });
  },

  waitForInit: function() {
    var div = document.createElement('div');
    div.id = 'waitingDiv';
    div.innerHTML = 'getting ready, please wait...';
    document.body.appendChild(div);
  },

  createKey: function() { // finds largest key
    var whenKeysLoaded = localforage.keys(); // returns Promise
    return whenKeysLoaded.then(function(keys) {
      // An array of all the key names.
      console.log('KEYS: ', keys);
      if (keys.length === 0) { // if no object is in the list, start at 0
        return ('0');
      } else {
        var intKeys = keys.map(function(obj){return Number.parseInt(obj)});
        var largestKey = Math.max.apply(null, intKeys);
        var newKey = (largestKey + 1).toString();
        return (newKey);
      }

    }).catch(function(err) {
      console.log(err);
    });
  }
}
export default LF;
