# API Dokumentation

 * [TileLoader](#markdown-header-tileloader)
 * [localForage](#markdown-header-localforage)

## TileLoader

Tile Loader, welcher das Speichern von Offline-Kacheln und deren Laden (mit Fallback auf Online-Kacheln) übernimmt.

### `new TileLoader(options)` Constructor

**Extends [EventEmitter](https://www.npmjs.com/package/eventemitter)**

Erzeugt einen Tile Loader für einen bestehenden Raster- oder VectorTile Layer.

Der Loader fügt eine `tileLoadFunction` hinzu, sodass Kacheln in der IndexedDB gespeichert oder von dieser gelesen werden können. Kacheln werden zuerst in der IndexedDB gesucht, und nur online angefragt, wenn sie dort nicht vorhanden sind.

**options**

 * `source {ol.source.UrlTile}` Vollständig konfigurierte Source für einen Raster- oder VectorTile Layer.

### Events

#### progress

Event zum Verfolgen des Fortschritts von [#seed()](#markdown-header-seedoptions-method). Wird jeweils nach 100 geladenen Kacheln emittiert.

**properties**

 * `etd {number}` Voraussichtliche Zeit bis zur Fertigstellung des Seedings in ms.
 * `percent {number}` Bisher geladene Kacheln in Prozent.
 * `tiles {number}` Anzahl der insgesamt zu ladenden Kacheln.

### `#seed(options)` Method

Laden von Kacheln für den Offline-Gebrauch. Während des Seed-Vorganges empfiehlt es sich, das Endgerät so einzustellen, dass es in keinen Stromspar- oder Ruhezustand schaltet.

**options**

 * `view {ol.View}` View zum automatischen Berechnen von `extent` und `minZoom`. Stattdessen kann `extent` und `minZoom` manuell angegeben werden.
 * `maxZoom {number}` Maximale Zoomstufe, für die Kacheln offline gespeichert werden sollen.
 * `extent {ol.Extent}` Extent, für den Kacheln offline gespeichert werden sollen. Nur notwendig, wenn `view` nicht angegeben wurde.
 * `minZoom {number}` Minimale Zoomstufe, für die Kacheln offline gespeichert werden sollen. Wenn nicht angegeben, wird `17` verwendet, was einer Auflösung von ca. 1m entspricht.

### `#deleteDatabase()` Method

Löschen der Offline-Datenbank. Nach dem Ausführen dieses Befehls muss die Applikation neu geladen werden.


## raster

Funktionen zur Verwendung mit `ol/source/Raster`. Erzeugt abgeleitete Datensätze aus Raster-Layern.

Die Funktionen aus diesem Module werden als `operation` einer `ol/source/Raster` konfiguriert und laufen als Web Workers. Parameter werden über das `data` Objekt des `beforeoperations` Events der Source übergeben, z.B.

```js
rasterSource.on('beforeoperations', function(event) {
  // the event.data object will be passed to operations
  var data = event.data;
  data.elevation = elevationFromPixel.toString();
  // ...
});
```

### `slope` Raster operation

Raster Operation für eine Hangneigungskarte.

**Parameter**

 * `resolution {number}` Auflösung. Wird üblicherweise auf die `resolution` des `beforeoperations` Events gesetzt.
 * `elevation {string}` String Repräsentation (`.toString()`) der Funktion, die aus einem rgba Array die Höhe zurückliefert.
 * `nodata {number}` Höhe, die ignoriert und transparent gerendert werden soll. Optional.
 * `colorMap {Object<number, Array<number>}` Objekt für den Style der Hangneigungskarte. Keys sind die Hangneigung in Prozent, Values ein rgb Array für Hangneigungswerte kleiner als die im Key angegebenen Prozent.

### `shade` Raster operation

Raster Operation für eine Geländeschummerung

**Parameter**

* `resolution {number}` Auflösung. Wird üblicherweise auf die `resolution` des `beforeoperations` Events gesetzt.
* `elevation {string}` String Repräsentation (`.toString()`) der Funktion, die aus einem rgba Array die Höhe zurückliefert.
* `nodata {number}` Höhe, die ignoriert und transparent gerendert werden soll. Optional.
* `vert {number}` Überhöhung.
* `sunEl {number}` Sonnenstand in Grad.
* `sunAz {number}` Azimut der Sonne in Grad.


## localForage

Modul zum Speichern und Abrufen von Vektordaten aus dem lokalen Speicher.


### `init()` Function

Methode zur Initialisierung des Moduls. Es wird mit [`navigator.storage.persist()`](https://developer.mozilla.org/en-US/docs/Web/API/StorageManager/persist) nach der Erlaubnis für persistente Speicherung der Daten gefragt (Chrome 55, Firefox 57, Opera 42).
Da die Initialisierung der localForage-Library je nach Gerät länger dauern kann, wird während des Initialisierungsvorgangs die Benutzeroberfläche inaktiv gestellt.
Als Driver wird ausschließlich IndexedDB verwendet.
Falls nicht bereits vorhanden, wird die Datenbank "LayersDataBase" mit dem data store "InitialStoreName" erstellt.

### `getItem(id)` Function

 * `id {string}` ID des gewünschten Vektorfeatures.

Methode zum Erhalten eines einzelnen Features als GeoJSON über dessen ID.

returns:

* `{Promise<string>}` GeoJSON des Features als string


### `addFeature(featureId, feature)` Function

 * `featureId {string}` gewünschte ID in IndexedDB.
 * `feature {string}` GeoJSON des Features als string.

Speichert einen String eines Features in IndexedDB.


### `createKey()` Function

Erzeugt die nächsthöchste verfügbare ID.

returns:

* `{Promise<string>}` nächsthöchste verfügbare ID

## DrawFeature

Erstellen und Editieren von Vektordaten mit Kartenlupe, Aufnahme von Fotos und speichern der veränderten Daten in IndexedDB.

### `new DrawFeature(options)` Constructor

**options**

 * `options {object}` Options
 * `options.map {module:ol/map}` OpenLayers Map (Hauptkarte)
 * `options.selectionContainer {HTMLDivElement}` Container der Geometrieauswahl
 * `options.zoomedMap {HTMLDivElement}` Container der Kartenlupe


### `snapping(snappingTrue)` Function

 * `snappingTrue {bool}` Snapping an/aus

Setzt die Snapping-Interaktion aktiv (snappingTrue == true) oder inaktiv (snappingTrue == false)


### `selecting(selectTrue)` Function

 * `selectTrue {bool}` Select an/aus

Setzt die Select-Interaktion aktiv (selectTrue == true) oder inaktiv (selectTrue == false). Bei (selectTrue == false) wird die Draw-Interaction wieder aktiviert und offene Select-Menüs werden gelöscht.


### `initInteractions()` Function

Fügt der Karte eine neue Draw- und Modify-Interaction des durch den User bei  DrawFeature.selectList gewählten Geometrietyps hinzu.


## LoadSources

Speichern und Laden der Layer über IndexedDB, Import und Export im GeoJSON-Format

### `new LoadSources(options)` Constructor

**options**

 * `options {object}` Options
 * `options.map {module:ol/map}` OpenLayers Map


### `initForm(container, layerNames) ` Function

* `container {HTMLDivElement}` Container für Layer-Selektion
* `layerNames {Array.<string>}` Array der Layernamen

Erstellt HTMLSelectElement aus layerNames und fügt es dem Container hinzu. Fügt ersten Layer der Karte hinzu.


### `loadLayer(storeName, createNewLayer) ` Function

* `storeName {string}` Name des IndexedDB-Stores des Layers (= Name des Layers)
* `createNewLayer {bool}` when true, erstellt neuen Layer

Ladet Layer aus IndexedDB in die Karte. Entfernt alte Layer (außer drawingLayer). Wenn (createNewLayer == false) werden die Features der vectorSource des drawingLayer hinzugefügt.

### `getAllStoreNames()` Function

Erzeugt ein Array mit allen Store Names der Datenbank "LayersDataBase" (= Layernamen)

returns:

* `{Promise<array.<string>>}` Array mit Layernamen


### `createRandomStores(amount)` Function

* `amount {number}` Anzahl der bereits existierenden Layer

Erzeugt für Demonstratioszwecke Beispiellayer und speichert diese in IndexedDB, sodass mindestens 3 Layer verfügbar sind.

### `exportLayer(storeName)` Function

* `storeName {string}` Name des Layers

Download eines Layers aus IndexedDB als GeoJSON. Der Dateiname ist der Layername.

### `importLayer(file)` Function

* `file {object}` GeoJSON File

Importiert GeoJSON File und speichert es in IndexedDB und fügt es zur selectList der Layer hinzu. Ist der Dateiname als Store Name bereits vorhanden, wird der Benutzer über ein confirm um Erlaubnis zum Überschreiben gefragt.
