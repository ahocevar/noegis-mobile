import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import Style from 'ol/style/Style';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Draw from 'ol/interaction/Draw';
import Snap from 'ol/interaction/Snap';
import Select from 'ol/interaction/Select';
import Circle from 'ol/style/Circle';
import Modify from 'ol/interaction/Modify';
import GeoJSON from 'ol/format/GeoJSON';
import LFModule from './localForage.js';
import DragPan from 'ol/interaction/DragPan';
import {singleClick} from 'ol/events/condition';

// function to be called after draw or modify, creates id (if necessary) and stores the feature
function drawendFunction(e) {
  if (e.feature.getId() === undefined) { // set unique id for drawn feature, if it doesnt have one
    let uniqueIdPromise = LFModule.createKey();
    uniqueIdPromise.then(function(newKey) {
      e.feature.setId(newKey);
      console.log('ID: ', e.feature.getId()); //eslint-disable-line
      storeFeature(e.feature);
    }).catch (function(err) {
      console.log ('could not create new key ', err); //eslint-disable-line
    });
  } else {
    storeFeature(e.feature);
  }
}

// store feature in indexedDB
function storeFeature(feature) {
  // can't store as ol.feature because of DOM-reference, compare: https://stackoverflow.com/questions/27584993/indexeddb-error-uncaught-datacloneerror-failed-to-execute-put-on-idbobjects#27585758
  let format = new GeoJSON();
  let featureGeoJSON = format.writeFeature(feature);
  //now add the feature to INDEXEDDB via LocalForage
  LFModule.addFeature(feature.getId().toString(), featureGeoJSON);
}

// delete attribute-popup if existing
function removePopUp() {
  if (document.getElementById('attributesDiv')) {
    document.getElementById('attributesDiv').outerHTML = '';
  }
}

function createIMG(dataURL) {
  let image = document.createElement('img');
  image.id = 'uploadImage';
  image.className = 'uploadImage';
  image.src =  dataURL;
  image.style.maxWidth = '100%';
  return image;
}

export default class DrawFeature {
  /**
   * @param {Object} options Options.
   * @param {module:ol/map} options.map OpenLayers map.
   * @param {HTMLDivElement} options.selectionContainer Container for then
   * geometry type selection.
   * @param {HTMLDivElement} options.zoomedMap target of the magnifying map.
   */
  constructor(options) {
    this.map = options.map;
    this.zoomedMap = options.zoomedMap;
    this.selectList = document.createElement('select'); // select for geometry type
    this.initForm(options.selectionContainer);

    this.vectorLayer = new VectorLayer({
      source: new VectorSource,
      style: new Style({
        fill: new Fill({
          color: 'rgba(255, 140, 0, 0.3)'
        }),
        stroke: new Stroke({
          color: '#FF8C00',
          width: 2
        }),
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#FF8C00'
          })
        })
      })
    });

    this.vectorLayer.set('name', 'drawingLayer');
    this.map.addLayer(this.vectorLayer);
  }

  test() {
    console.log('Module DrawFeature is working correctly!'); //eslint-disable-line
  }

  snapInteraction() {
    return new Snap({
      source: this.vectorLayer.getSource()
    });
  }

  snapping(snappingTrue) {
    if (snappingTrue) {
      console.log('adding snapping'); //eslint-disable-line
      this.map.addInteraction(this.snapInteraction());
    } else {
      console.log('removing snapping'); //eslint-disable-line
      this.map.removeInteraction(this.snapInteraction());
    }
  }

  initSelectInteraction() {
    console.log('init select interaction');

    this.select = new Select();
    this.map.addInteraction(this.select);
    this.select.on('select', function(e) {
      removePopUp();
      console.log(e.selected[0]);
      if (typeof e.selected[0] === 'undefined') {
        console.log('id undefined');
        return;
      }
      console.log('select ID: ', e.selected[0].getId()); //eslint-disable-line
      this.createAttributeWindow(e.selected[0]);
    }.bind(this));
    this.select.setActive(false);
  }

  selecting(selectTrue) {
    if (this.select == undefined) {
      this.initSelectInteraction();
    }
    console.log(this.select);
    if (selectTrue) {
      console.log('add selecting'); //eslint-disable-line
      this.select.setActive(true);
      this.draw.setActive(false);
      this.modify.setActive(false);
    } else {
      console.log('removing selecting'); //eslint-disable-line
      removePopUp();
      this.select.setActive(false);
      this.draw.setActive(true);
      this.modify.setActive(true);
      // deselect all features
      this.select.getFeatures().clear();
    }
  }

  // create and populate div for editing attributes of selected feature
  createAttributeWindow(element) {
    let id = element.getId();
    console.log('creating div for editing feature id: ', id);
    let div = document.createElement('div');
    div.id = 'attributesDiv';
    div.className = 'editAttributes';

    let idDiv = document.createElement('div');
    idDiv.id = 'idDiv';
    idDiv.className = 'attributeDiv';
    idDiv.innerHTML = 'ID:   ' + id;
    div.appendChild(idDiv);

    // div for pre-existing pictures
    // show images, if feature already has some
    let existingImages = element.get('images');
    if (existingImages) {
      existingImages.forEach(dataURL => {
        let oldFotoDiv = document.createElement('div');
        oldFotoDiv.id = 'oldFotoDiv';
        oldFotoDiv.className = 'attributeDiv';
        let image = createIMG(dataURL);
        oldFotoDiv.appendChild(image);
        div.appendChild(oldFotoDiv);
      });
    }

    let uploadDiv = document.createElement('div');
    uploadDiv.id = 'uploadDiv';
    uploadDiv.className = 'attributeDiv';
    let input = document.createElement('input');
    input.type = 'file';
    input.style.overflow = 'auto';
    input.accept = 'image/*';
    input.capture = 'camera';
    input.onchange = event => {
      let fileList = input.files;
      //console.log(fileList[0]);
      let reader = new FileReader();
      reader.onload = () => {
        let dataURL = reader.result;
        let image = createIMG(dataURL);
        let fotoDiv = document.createElement('div');
        fotoDiv.id = 'fotoDiv';
        fotoDiv.className = 'attributeDiv';
        fotoDiv.appendChild(image);
        div.appendChild(fotoDiv);
        //get promise that returns geoJSON from INDEXEDDB from selected Feature
        let itemPromise = LFModule.getItem(id);
        itemPromise.then((item) => {
          let format = new GeoJSON();
          let feature = format.readFeature(item);
          let properties = feature.getProperties();
          console.log(properties);
          if (!properties.hasOwnProperty('images')) {
            feature.setProperties({images: [dataURL]});
          } else {
            properties.images.push(dataURL);
          }
          console.log(feature);
          let newFeatureGeoJSON = format.writeFeature(feature);
          //replace existing feature in INDEXEDDB with new one
          LFModule.addFeature(id, newFeatureGeoJSON);
          // replace existing feature in Layer Source with new one
          console.log('layer source: ');
          let layerArray = this.map.getLayers().getArray();
          layerArray.some(layer => {
            if (layer.get('name') === 'drawingLayer') {
              let oldFeature = layer.getSource().getFeatureById(id);
              let oldFeatureProperties = oldFeature.getProperties();
              if (!oldFeatureProperties.hasOwnProperty('images')) {
                oldFeature.setProperties({images: [dataURL]});
              } else {
                oldFeatureProperties.images.push(dataURL);
              }
              return true;
            }
          })
        });
      };
      reader.readAsDataURL(fileList[0]);
    };
    uploadDiv.appendChild(input);
    div.appendChild(uploadDiv);

    console.log('pre-existing properties:');
    console.log(element.getProperties());

    document.body.appendChild(div);
  }

  mapHasDragPan() {
    let dragPanDetected = false;
    this.map.getInteractions().forEach(function(interaction) {
      if (interaction instanceof DragPan && interaction.getActive() === true) {
        console.log('dragpan detected'); //eslint-disable-line
        this.dragPan = interaction;
        dragPanDetected = true;
      }
    }.bind(this));
    return dragPanDetected;
  }

  // create and populate Form for selecting geometry type to be drawn
  initForm(htmlContainer) {
    let geometryArray = ['Polygon', 'LineString', 'Point'];
    for (var i = 0, len = geometryArray.length; i < len; i++) {
      geometryArray[i];
      var option = document.createElement('option');
      option.value = geometryArray[i];
      option.text = geometryArray[i];
      this.selectList.appendChild(option);
    }
    htmlContainer.appendChild(this.selectList);
  }

  // create new draw interaction from selected geomety type
  initInteractions() {
    console.log('initializing interactions'); //eslint-disable-line
    console.log('selectList Value: ', this.selectList.value); //eslint-disable-line
    this.draw = new Draw({
      source: this.vectorLayer.getSource(),
      type: this.selectList.value,
      stopClick: true
    });
    this.draw.on('drawend', drawendFunction);

    this.map.addInteraction(this.draw);

    this.modify = new Modify({
      deleteCondition: singleClick,
      source: this.vectorLayer.getSource()
    });
    //call drawendfunction for each modified feature at modifyend
    this.modify.on('modifyend', function(e) {
      e.features.getArray().forEach(function(feature) {
        e.feature = feature;
        drawendFunction(e);
      })
    });
    this.map.addInteraction(this.modify);

    const target = this.map.getTargetElement();

    const pointerup = (function(e) {
      console.log(this.timeoutInstance);
      console.log('hide zoomed map');
      clearTimeout(this.timeoutInstance);
      delete this.timeoutInstance; // has to be deleted completely, make new instance at pointerdown
      this.zoomedMap.style.display = 'none';
    }).bind(this);


    const pointerdown = (function(e) {
      console.log(typeof this.timeoutInstance);
      if (typeof this.timeoutInstance === 'undefined') {
        console.log('creating timeout function');
        this.timeoutInstance = setTimeout(function() {
          console.log('show zoomed map');
          this.zoomedMap.style.display = 'block';
        }.bind(this), 500);
      }
    }).bind(this);

    target.addEventListener('mousedown', pointerdown);
    target.addEventListener('touchstart', pointerdown);

    // pointerup listener has to be independent from pointerdown for line and points
    target.addEventListener('mouseup', pointerup);
    target.addEventListener('touchend', pointerup);
  }
}
