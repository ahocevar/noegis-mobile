import 'ol/ol.css';
import 'ol-layerswitcher/src/ol-layerswitcher.css';
import GeoJSON from 'ol/format/GeoJSON';
import Map from 'ol/CanvasMap';
import Raster from 'ol/source/Raster';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import ImageLayer from 'ol/layer/Image';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import Vector from 'ol/source/Vector';
import View from 'ol/View';
import XYZ from 'ol/source/XYZ';
import OSM from 'ol/source/OSM';
import {transformExtent} from 'ol/proj';
import {createXYZ} from 'ol/tilegrid';
import LayerSwitcher from 'ol-layerswitcher';
import {slope, shade} from '../src/raster';

const tileLayer = new TileLayer({
  title: 'OSM',
  source: new OSM()
});

function elevationFromPixel(pixel) {
  return -10000 + (pixel[0] * 256 * 256 + pixel[1] * 256 + pixel[2]) * 0.1;
}

const elevation = new XYZ({
  url: 'data/dtm_10x10/{z}/{x}/{-y}.png',
  tileGrid: createXYZ({
    minZoom: 8,
    maxZoom: 14
  }),
  crossOrigin: 'anonymous',
  transition: 0
});
const elevationExtent = transformExtent([9.31850899612, 46.2978863769, 17.3625692412, 49.0909015047], 'EPSG:4326', 'EPSG:3857');
const demLayer = new TileLayer({
  opacity: 0,
  source: elevation,
  extent: elevationExtent
});

const hillshadeSource = new Raster({
  sources: [elevation],
  operationType: 'image',
  operation: shade
});
hillshadeSource.on('beforeoperations', function(event) {
  // the event.data object will be passed to operations
  var data = event.data;
  data.resolution = event.resolution;
  data.elevation = elevationFromPixel.toString();
  data.vert = 1;
  data.sunEl = 45;
  data.sunAz = 45;
  data.nodata = 0;
});
const hillshadeLayer = new ImageLayer({
  type: 'base',
  title: 'Hillshade',
  opacity: 0.3,
  source: hillshadeSource,
  extent: elevationExtent
});

const slopeColorMap = {
  20: [0, 255, 0],
  30: [255, 255, 0],
  40: [255, 0, 0],
  100: [255, 0, 255]
};
const slopeSource = new Raster({
  sources: [elevation],
  operationType: 'image',
  operation: slope
});
slopeSource.on('beforeoperations', function(event) {
  // the event.data object will be passed to operations
  var data = event.data;
  data.resolution = event.resolution;
  data.elevation = elevationFromPixel.toString();
  data.colorMap = slopeColorMap;
  data.nodata = 0;
});
const slopeLayer = new ImageLayer({
  type: 'base',
  title: 'Hangneigung',
  opacity: 0.3,
  source: slopeSource,
  extent: elevationExtent
});

const noeLayer = new VectorLayer({
  title: 'Landesgrenze',
  source: new Vector({
    url: 'data/noe-landesgrenze.geojson',
    format: new GeoJSON()
  }),
  style: new Style({
    stroke: new Stroke({
      color: 'fuchsia',
      width: 2
    })
  })
});
let noeExtent;

const map = new Map({
  target: 'map',
  layers: [demLayer, tileLayer, slopeLayer, hillshadeLayer, noeLayer],
  view: new View({
    zoom: 2,
    center: [0, 0]
  })
});
map.addControl(new LayerSwitcher());

let view;
noeLayer.getSource().once('change', function(e) {
  noeExtent = e.target.getExtent();
  view = new View({
    extent: noeExtent,
    minZoom: 8,
    maxZoom: 14
  });
  map.setView(view);
  view.fit(noeExtent, {constrainResolution: false});
});

map.on('click', function(e) {
  map.forEachLayerAtPixel(e.pixel, function(layer, data) {
    document.getElementById('elevation').innerHTML = elevationFromPixel(data).toFixed(2);
  }, undefined, function(layer) {
    return layer === demLayer;
  });
});
