import 'ol/ol.css';
import {getTopLeft} from 'ol/extent';
import GeoJSON from 'ol/format/GeoJSON';
import Map from 'ol/CanvasMap';
import MVT from 'ol/format/MVT';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import TileLayer from 'ol/layer/Tile';
import VectorTileLayer from 'ol/layer/VectorTile';
import VectorTile from 'ol/source/VectorTile';
import VectorLayer from 'ol/layer/Vector';
import Vector from 'ol/source/Vector';
import View from 'ol/View';
import XYZ from 'ol/source/XYZ';
import {get as getProj, transformExtent} from 'ol/proj';
import TileLoader from '../src/TileLoader';
import TileGrid from 'ol/tilegrid/TileGrid';
import {createXYZ} from 'ol/tilegrid';
import sync from 'ol-hashed';

const progressBasemap = document.getElementById('progress-basemap');
const progressWidmung = document.getElementById('progress-widmung');
const seedBasemap = document.getElementById('seed-basemap');
const seedWidmung = document.getElementById('seed-widmung');
const clearBasemap = document.getElementById('clear-basemap');
const clearWidmung = document.getElementById('clear-widmung');

const tileLayer = new TileLayer({
  source: new XYZ({
    url: 'https://maps{1-4}.wien.gv.at/basemap/geolandbasemap/normal/google3857/{z}/{y}/{x}.png',
    tileGrid: new TileGrid({
      extent: [977649.9582335392, 5838029.951202585, 1913529.9492146818, 6281289.924879572],
      origin: [-20037508.3428, 20037508.3428],
      resolutions: createXYZ({
        maxZoom: 18
      }).getResolutions()
    })
  })
});

const fill = new Fill();
const style = new Style({
  fill: fill
});

const widmungLayer = new VectorTileLayer({
  opacity: 0.7,
  maxResolution: 305.74811314070504,
  source: new VectorTile({
    transition: 0,
    url: 'data/widmung/{z}/{x}/{y}.pbf',
    format: new MVT(),
    tileGrid: new TileGrid({
      minZoom: 10,
      resolutions: createXYZ({
        tileSize: 512,
        maxZoom: 15
      }).getResolutions(),
      tileSize: 512,
      origin: getTopLeft(getProj('EPSG:3857').getExtent()),
      extent: transformExtent([14.452636, 47.422186, 17.068835, 49.020526], 'EPSG:4326', 'EPSG:3857')
    })
  }),
  style: (feature) => {
    const widmung = feature.get('WIDMUNG');
    if (widmung && widmung.indexOf('Bauland') == 0) {
      fill.setColor('#f0826a');
    } else {
      fill.setColor('#fdfdf9');
    }
    return style;
  }
});

const noeLayer = new VectorLayer({
  source: new Vector({
    url: 'data/noe-landesgrenze.geojson',
    format: new GeoJSON()
  }),
  style: new Style({
    stroke: new Stroke({
      color: 'fuchsia',
      width: 2
    })
  })
});
let noeExtent;

const tileLoaderBasemap = new TileLoader({
  name: 'basemap',
  source: tileLayer.getSource()
});
tileLoaderBasemap.on('progress', e => {
  progressBasemap.innerHTML = `${e.percent}% von ${e.tiles} basemap Kacheln geladen. Fertig in ${Math.floor(e.etd / 60)}h ${e.etd % 60}min.`;
});

const tileLoaderWidmung = new TileLoader({
  name: 'widmung',
  source: widmungLayer.getSource()
});
tileLoaderWidmung.on('progress', e => {
  progressWidmung.innerHTML = `${e.percent}% von ${e.tiles} widmung Kacheln geladen. Fertig in ${Math.floor(e.etd / 60)}h ${e.etd % 60}min.`;
});

const map = new Map({
  target: 'map',
  layers: [tileLayer, noeLayer, widmungLayer],
  view: new View({
    zoom: 2,
    center: [0, 0]
  })
});

let view;
noeLayer.getSource().once('change', function(e) {
  noeExtent = e.target.getExtent();
  view = new View({
    extent: noeExtent
  });
  map.setView(view);
  view.fit(noeExtent, {constrainResolution: false});
  sync(map);
});

seedBasemap.onclick = function() {
  tileLoaderBasemap.seed({view: view});
};
seedWidmung.onclick = function() {
  tileLoaderWidmung.seed({
    view: view,
    minZoom: 10
  });
};
clearBasemap.onclick = function() {
  tileLoaderBasemap.deleteDatabase();
};
clearWidmung.onclick = function() {
  tileLoaderWidmung.deleteDatabase();
};
