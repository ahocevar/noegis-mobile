#!/bin/sh
download() {
    if command -v wget 2>/dev/null; then
        wget "$1" -O "$2"
    else
        curl -o "$2" "$1"
    fi
}

BASEDIR=$(pwd)
mkdir -p build/examples/data
cd build/examples/data

# Download DEM and create tiles
download "http://open-data.noe.gv.at/ogd-data/BD3/DTM_10x10.zip" "DTM_10x10.zip"
unzip DTM_10x10.zip
python $BASEDIR/dem2rgb.py DTM_10x10.tif dtm_10x10.rgb.tif
gdal2tiles.py -z8-14 dtm_10x10.rgb.tif dtm_10x10

# Download Widmungsumhüllende and create tiles
download "https://sdi.noe.gv.at/OGD/at.gv.noe.geoserver/wfs?request=GetFeature&version=1.1.0&typeName=OGD:RRU_WI_HUELLE&srsName=EPSG:4326&outputFormat=application/json" "widmung.json"
tippecanoe --no-tile-compression --minimum-zoom=10 --maximum-zoom=15 --detect-shared-borders widmung.json --output-to-directory=widmung -f

# Cleanup
rm DTM_10x10.zip
rm DTM_10x10.tif
rm DTM_10x10.tif.ovr
rm dtm_10x10.rgb.tif
rm widmung.json
